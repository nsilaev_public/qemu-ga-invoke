# qemu-ga-invoke

### Invoke a Linux command with qemu-guest-agent.


```bash
KVM# ./invoke <domain> <linux command>
```

